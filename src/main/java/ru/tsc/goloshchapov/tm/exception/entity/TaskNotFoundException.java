package ru.tsc.goloshchapov.tm.exception.entity;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Exception! Task is not found!");
    }

}
