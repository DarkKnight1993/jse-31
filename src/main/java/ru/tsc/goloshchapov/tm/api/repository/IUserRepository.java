package ru.tsc.goloshchapov.tm.api.repository;

import ru.tsc.goloshchapov.tm.api.IRepository;
import ru.tsc.goloshchapov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUser(User user);

    User removeByLogin(String login);

}
