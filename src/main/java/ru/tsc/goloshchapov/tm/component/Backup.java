package ru.tsc.goloshchapov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.data.BackupLoadCommand;
import ru.tsc.goloshchapov.tm.command.data.BackupSaveCommand;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    private static final int INTERVAL = 30000;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    private void begin() {
        executorService.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.MILLISECONDS);
    }

    public void init() {
        load();
        begin();
    }
    public void stop() {
        executorService.shutdown();
    }

    public void load() {
        bootstrap.runCommand(BackupLoadCommand.NAME);
    }

    public void save() {
        bootstrap.runCommand(BackupSaveCommand.NAME);
    }

}
