package ru.tsc.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.exception.empty.EmptyNameException;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@Nullable Project project) {
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("[SELECTED PROJECT]");
        System.out.println(
                "Id: " + project.getId() +
                        "\nName: " + project.getName() +
                        "\nDescription: " + project.getDescription() +
                        "\nStatus: " + project.getStatus().getDisplayName()
        );
        System.out.println("[END PROJECT]");
    }

    @NotNull
    protected Project createProject(@Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return new Project(name, description);
    }

}
