package ru.tsc.goloshchapov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpShowCommand extends AbstractSystemCommand {
    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String description() {
        return "Display help-list with possible app commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (final AbstractCommand command : commands) System.out.println(command);
    }

}
