package ru.tsc.goloshchapov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {
    @NotNull
    @Override
    public String name() {
        return "project-show-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by index";
    }

    @Override
    public void execute() {
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT INFO BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final Project project = serviceLocator.getProjectService().findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
