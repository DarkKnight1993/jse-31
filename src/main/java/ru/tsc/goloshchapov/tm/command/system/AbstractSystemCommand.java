package ru.tsc.goloshchapov.tm.command.system;

import org.jetbrains.annotations.Nullable;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected void showCommandValue(@Nullable final String value) {
        if (value == null || value.isEmpty()) return;
        System.out.println(value);
    }

}
